+++
title = "Welcome to the TechLit Wiki"
linktitle = "Welcome"
toc = false
weight = 1
+++

The TechLit system is a customized Linux distribution based on Artix Linux.
Tailored specifically for the curriculum needs of TechLit, one of its key
features is the use of btrfs snapshots, which allow for easy iteration and
reproducibility.

# Guide Overview

## [Startup Guide](docs/get_started)

Learn how to install and update the TechLit system using the provided USB drive.

## [Updating](docs/updating)

Keep your System and components up-to-date via
- [bastion](docs/updating)
- Classroom [Network](docs/updating/local)

## [Desktop Management](docs/desktop)

- Setting up your local [Network](docs/desktop/network)
- [asdf](docs/desktop/asdf)

## [Advanced](docs/advanced)

- Set up your [router](docs/advanced/router)
- Fix your [USB](docs/advanced/usb)

## [FAQ](docs/faq)

Find answers to the most frequently asked questions about the system.

{{< cards cols = "1" >}}
 {{< card link="docs/get_started" title="Get Started" icon="cursor-click" >}}
{{< /cards >}}
