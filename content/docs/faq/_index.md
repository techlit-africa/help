+++
title = "Frequently Asked Questions"
linktitle = "FAQ"
weight = 8
+++

{{% details title="How do stop the firewall temporarily?" closed="true" %}}

>[!CAUTION]
You are responsible for your classroom security,
only stop the firewall if you know what you are doing.
Reboot or re-enable the firewall after your session.

`rsv stop nftables`
{{% /details %}}

{{% details title="My computer is slow/laggy after updating" closed="true" %}}

Switch or enable a GPU hacks in
[reconfigure](../desktop#system-configuration-with)

{{% /details %}}

{{% details title="How do I mount my USB drive?" closed="true" %}}

```bash
# Identify the USB drive
lsblk

# Mount the 4th partition of the USB drive
sudo mount /dev/sdx4 /mnt
```

{{% /details %}}

{{% details title="I get component not installed." closed="true" %}}

`mkdir /srv/component`

{{% /details %}}

{{% details title="How do i login as admin" closed="true" %}}

`su admin`
pass : `empowerwatoto`

{{% /details %}}

{{% details title="How do i switch to TTY2" closed="true" %}}

- On most computers <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>F2</kbd>
- On Apple computers <kbd>CMD</kbd> + <kbd>Option</kbd> + <kbd>F2</kbd>
- Rare cases <kbd>FN</kbd> + <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>F2</kbd>
- As admin `sudo chvt 2`
{{% /details %}}

{{% details title="How do i Fix wrong client hostnames" closed="true" %}}

Use [reconfigure](../desktop#system-configuration-with)
{{% /details %}}

## Wifi Issues

### I am unable to turn on my WIFI

- Run `rfkill list` to see all your network devices.
- If your WiFi is listed as soft blocked, run `sudo rfkill unblock wifi`
to turn it on.
- If it is hard blocked, look for a hardware switch on your computer
and turn it on.
- If none of these work, try a different wifi hack in [reconfigure](../desktop#system-configuration-with)

## Device Specific Issues

### ==Dell XPS / Dell Mini==

Having trouble with your Dell XPS or Dell Mini?
Check out these quick fixes for common boot issues:

#### My Dell XPS won’t boot from my USB

> If you see the error **Selected boot device failed.
Press any key to reboot the system**, try the following steps

- **First Try:** Plug the USB into a different port on your computer.
Sometimes, a simple switch is all it takes!
- **Next Steps:** If that doesn’t work, let’s get your USB on the boot list manually:
  1. Restart your computer and press `F2` or `F12` to enter the BIOS setup.
  2. Go to “General” and select “Boot Sequence”.
     ![Add boot](dell_xps_add.png "Boot list")
  3. Click on “Add Boot Option” and choose your USB device.
  4. Navigate to \EFI\BOOT\ and select the `BOOTX64.EFI` file.
     ![Boot img](dell_xps_boot_img.jpg "Boot list")
  5. Save your changes and exit the BIOS setup.
  Try booting up again and choose your USB as the boot device.
     ![Boot list](dell_xps_boot_list.jpg "Boot list")

#### Not detecting My USB`3.x` drive

> You might need to switch up the boot mode. Here’s how

  1. Enter BIOS setup by pressing `F2` or `F1` during startup.
  2. Look for the boot settings and change the boot mode from
  BIOS or Legacy to UEFI mode.
