+++
title = "Downloading youtube videos"
linktitle = "Youtube"
weight = 1
draft = false
+++

Downloading tutorials from YouTube videos can be done through various methods.
This guide covers two reliable options: using a browser add-on
or the yt-dlp tool.

1. Using [browser addon](#using-browser-addon)
2. Using [`yt-dlp`](#using-yt-dlp-recommended) (recommendeded)

## Prerequisites

- **Connection**: Connect to your phone's hotspot/USB or school Wi-Fi
- **Firewall**: Temporarily stop the firewall with `rsv stop nftables`
- **Usage**: Preferably use the server. If you cannot connect to your
hotspot/USB, use any client

## Using browser addon

{{< callout type="warning" >}}

- **Disclaimer**: the addon is not maintained, endorsed, or affiliated by the
author or TechLit. Visit the site [home](https://yourvideofile.org)
for more details.
- **NOTE**: You might need to set the date manually using the `date`
command: `sudo date -s "YYYY-MM-DD HH:MM:SS"`

{{< /callout >}}

{{% steps %}}

### Launch firefox from start menu

### Install the extension

Install the Easy YouTube Downloader extension available
[here](https://addons.mozilla.org/en-US/firefox/addon/easy-youtube-video-download).

![addon](addon.png "Youtube downloader")

### Open the Video link

Open the [video link](https://www.youtube.com/watch?v=Qg1ml3aHAM4)
you wish to download

### Download the video

You should see a download button below the video title, next to the subscribe
button. Click on the download button and select your preferred quality.

![Download](download.png "Download")
> The download should start shortly

### Move the video

Copy or move the video from downloads folder to public storage to
avoid losing it after logout.

{{% /steps %}}

## Using yt-dlp (Recommended)

{{% steps %}}

### Install yt-dlp

```sh
sudo pacman -Syy
sudo pacman -S yt-dlp
```

### Change to your desired directory

`cd /srv/public/teachers/teacher-01`

### Initiate download

```bash
yt-dlp -f best https://www.youtube.com/watch?v=Qg1ml3aHAM4

# Or simply
yt-dlp -f best Qg1ml3aHAM4
```

{{% /steps %}}
{{< callout type="info" >}}

- You can list formats available using `yt-dlp -F url` and choose
the format you wish to download in the format `yt-dlp -f video+audio url`
{{< /callout >}}

{{< cards >}}
 {{< card link="../advanced" title="Advanced" icon="arrow-left" >}}
 {{< card link="../faq" title="FAQ" icon="arrow-right" >}}
{{< /cards >}}
