+++
title = "Provision USB"
tags = "Installer"
weight = 4
+++

Here’s how to repair your broken USB drive.

{{< callout type="error" >}}

- This will destroy everything on the USB drive.
- Do this on a computer with the latest desktop downloaded for ease.
- Only do this if you are confident your USB is broken.
{{< /callout >}}

{{% steps %}}

### Download recovery

Download the recovery image from bastion if you don't have it locally
see [this](../../desktop/updating/downloading) guide for downloading.

### Confirm version

Confirm you have the latest version of recovery image with

```bash
tl-comp-latest recovery
```

### Plug in the USB drive

Identify the usb drive on the computer using `lsblk`

### Repair your USB

{{< callout >}}
Say your USB drive is `/dev/sdb` from the previous step
{{< /callout >}}

```bash
tl-usb-prepare /dev/sdb
```

### Unplug your drive

{{% /steps %}}

Your USB is ready for use see [booting](../../get_started)
