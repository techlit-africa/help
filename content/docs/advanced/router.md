+++
title = "Router Configuration"
linktitle = "Router"
weight = 2
draft = false
+++


{{% details title="__Click here to see pre-requisites__" closed="true" %}}

- Your router has been __reset__ to its original factory settings.
- You may use any __client__ to configure the router.
- Configuring __multiple routers__? see the [table](#multiple-routers-scenario) below

{{% /details %}}

{{% details title="__Click here to see available options__" closed="true" %}}

- __Admin password__ = `empowerwatoto1`
- __SSID__ = TechLit Africa/Vim/Emacs
- __Operation Mode__ = AP
- __Network__ = Static IP
- __Subnet__ = 192.168.1.0
- __Mask__ = 255.255.255.0
- __DHCP__ = disabled
- __Width__ = 40 MHz
- __Channel__ = See the table below [table](#multiple-routers-scenario)
- __IP__ = See [table](#multiple-routers-scenario)
- __Password__ = `empowerwatoto`
- __TX Power__ = Low

{{% /details %}}

## How to configure the router

{{% steps %}}

### Disconnect the router

Disconnect all ethernet connections from the router,
disconnect the server if connected(using wifi).

### Connect a client to the router

- Look for an SSID named `TP-Link_xxx` in your Wi-Fi network list.
- The default password is usually found on a label beneath the router.

### Open interface

Open the router's web interface in a web browser __here__:
[192.168.0.1](http://192.168.0.1)

- Enter `192.168.0.1` in your web browser to access the router's settings.
- You'll be prompted to set a new admin password for security purposes.
- The router's interface should now be visible to you.

![Web interface](web_interface.png "tp-link Web Interface")

### Configure Operation Mode

To change __Operation Mode__
    - Select `Operation Mode` from the side menu.
    - Choose `Access Point` mode, save the changes,
and wait for the router to reboot.
![Operation mode](ap_mode.png "Router Operation mode")

### Configure SSID, Width and Channel

To change the __SSID__, __Width__ and __Channel__

- Navigate to `Wireless` settings in the side menu.
- Set the `Wireless Network Name` to your desired SSID.
- Adjust `Channel Width` to __40 MHz__ for better performance.
- Set the `Channel` to Auto or if using multiple routers,
as per the provided [table](#multiple-routers-scenario).
- Save the changes and reconnect using the new SSID with the previous password.

![Wireless Settings](wireless.png "Router wireless settings")

### Set Password

- Go to `Wireless Security` settings under the `Wireless` menu.
- Choose `WPA/WPA2 - Personal` and enter the wifi password.
- Save and recconnect the SSID using the new password

![Wireless](wireless_security.png "Router wireless settings")

### Configure IP and Gateway

- Select `Network` from the main menu and choose `LAN` settings.
- Opt for `Static IP` and input the correct IP address.
- Set the `Gateway` to 192.168.1.1
- Save the settings and let the router reboot.

![Network](network_ip.png "Router wireless settings")

### Disable DHCP

- After setting the new IP, access the web interface using the new address
(e.g., `192.168.1.100`).
- Log in with the new IP and admin password.
- Go to `DHCP` settings and click on `Disable`.

![DHCP](dhcp.png "Router DHCP settings")

{{% /steps %}}

{{< callout type="warning" >}}

- Once you have DHCP disabled, you will need to the router connect it to server
and [reconfigure](../../desktop#post-install)
{{< /callout >}}

</br>

---

### Multiple Routers scenario

We have standardized the configuration for 6 Access Points.
Below are the settings for each AP:

| SSID                  | IPv4 Address | Channel |
|-----------------------|--------------|---------|
| TechLit Vim           | 192.168.1.100| 2       |
| TechLit Emacs         | 192.168.1.102| 10      |
| TechLit Vim Yank      | 192.168.1.100| 2       |
| TechLit Vim Paste     | 192.168.1.101| 10      |
| TechLit eMacs Mark    | 192.168.1.102| 2       |
| TechLit eMacs Point   | 192.168.1.103| 10      |
