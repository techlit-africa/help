+++
title = "Pulling Updates within TechLit Network"
linktitle = "Local Updates"
weight = 4
+++

This guide will show you how to download and install components
within a TechLit environment, using local network.

### 0. Download updates

- Say `tl-1234` has a curriculum update.

>[!WARNING] Ensure you are on TechLit Wi-Fi and not on your mobile data.

{{% steps %}}

### Check connection

Confirm the computer with the updates is online with `ping tl-1234.local`

### Download

Download update with

```bash
tl-comp-pull  curriculum  tl-1234.local
```

> You should see that version __downloaded__

### Confirm download

Confirm the update is on the new computer now with

```bash
tl-comp-latest curriculum
```

### Update

Follow the steps to [install](../components)
the update on the new computer.

{{% /steps %}}
