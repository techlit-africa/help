+++
title = "Updating components from bastion"
linktitle = "Updating"
weight = 2
+++

{{% details title="__READ ME first__" closed="true" %}}

1. Before starting, make sure to run the shell as an admin,
either by using `su - admin` or through the Admin Terminal app."
2. Components include, but are not limited to,
`desktop`, `admin`, `curriculum`, `help` `control`
3. Replace __component__ and __version__ with the ones you need.
4. After downloading the updates from the bastion server, you can pull them
[locally](../updating/local)
{{% /details %}}

> [!TIP]
> Update your admin to `0.12.0`+ [See why!](#quick-update-command)

## Prerequisites

{{% steps %}}

### Connect

Connect a TechLit client to your mobile data using either via USB or Wi-Fi.

### Identify the component(s)

Locate the files you want to download in the browser from
the bastion [server](http://artix.techlitafrica.org).

### Quick update command

If you have admin __v0.12.0+__ installed, use this
`tl-comp-pull component http://bastion`

> Skip the rest of this section

### Stop the firewall

Temporarily stop the firewall with `rsv stop nftables`

### Download the files

>[!important]
> In this example, we are using curriculum version 0.1.310,
but this process applies to all components and versions

```bash
# Navigate to the directory
cd /srv/curriculum/

# Download the update files
aria2c  -Z  http://artix.techlitafrica.org/curriculum/v0.1.310.{tar.gz,sha256,changelog}

# Confirm the files are not corrupt
sha256sum  -c  v0.1.310.sha256
```

{{% /steps %}}

### Next

{{< cards cols="1" >}}
 {{< card link="components" title="Installing" icon="arrow-right" >}}
{{< /cards >}}
