+++
title = "Updating from USB"
linktitle = "USB"
weight = 4
+++

### Update System by Transferring Files from USB

{{% steps %}}

### Delete old files

> You'll need to use your judgement here

```bash
# Check available files
ls /srv/desktop

# Delete old files
# Say you want to delete v18,19,20
rm /srv/desktop/v{18,19,20}.*
```

### Mount USB and Transfer New Files

```bash
# Identify the USB device
lsblk

# Assume the USB device is /dev/sdb
sudo mount /dev/sdb4 /mnt

# Transfer new files
rsync -avP /mnt/desktop/v* /srv/desktop
```

### Unmount USB and unplug it

```bash
# Unmount USB 
sudo umount /mnt

```

### Update the desktop

```bash
# Update Desktop and reboot
tl-img-unpack && tl-img-restore

sudo reboot
```

{{% /steps %}}
