+++
title = "Installing Desktop Updates"
linktitle = "Desktop"
weight = 3
+++

## Desktop

>[!WARNING]
> - It is crucial to install `admin` before updating desktop.
> - Installing `Mini?` Follow [this](#minis) guide.

{{% details title="Understanding Versioning Requirements" closed="true" %}}

When __updating__ to a new version of our Desktop
certain previous versions are required to be installed. Here’s how it works:

- __Minor Updates__ (e.g., 20.0.1):
        - You need the base version (e.g., 20.0.0) installed.
- __Feature Updates__ (e.g., 20.1.0):
        - You need the base version (e.g., 20.0.0) installed.
- __Patch Updates__ (e.g., 20.1.2):
        - You need the base version (e.g., 20.0.0) and the most recent
  feature update (e.g., 20.1.0) installed.

If the necessary versions are already installed, no need to download them again.
{{% /details %}}

{{% steps %}}

### Confirm the update is avialable

```bash
tl-comp-latest desktop
```

### Unpack the update

```bash
tl-img-unpack
```

### Install the update

```bash
tl-img-restore
```

### Reboot to use the new system

{{% /steps %}}

## Minis

The `-mini` desktop versions are for computers with less than 32gb drive.

>[!warning]
> - You can only do this by booting from your USB drive.
> - Your drive needs to have at least 10gb of free space for unpacking
> - _delete the other desktop versions_

### Prerequisites

{{% steps %}}

### Download the update(s)

> If you haven't already

1. Pick a working computer with admin installed
2. Follow [this](../downloading) to download `-mini`
and admin updates to your desktop if you haven't already

### Copy the downloaded file to the USB drive

```bash
# Mount usb drive
sudo mount /dev/sdx4 /mnt


# Copy the downloaded files (repace ver with the version number)
rsync -avP /srv/desktop/ver-mini.* /mnt/desktop
rsync -avP /srv/admin/ver.* /mnt/admin

# Unmount the USB drive
sudo umount /mnt
```

### Boot the USB drive

Boot the usb drive on the target computer `ESC` for
[chromebooks](../../../get_started)

{{% /steps %}}

### Updating using the new admin

> You need admin version `0.11.15`+ to do this

{{% steps %}}

### Update admin in the installer

```bash
tl-archive-install admin
```

### Update or Prepare the system

Run the update or prepare the system using the `STAGE`
environment variable pointing to the unpack directory

```bash
# Run this if Techlit System is already installed
tl-hw-update-desktop /dev/mmcblk0

# if TechLit system isn't installed or the other command failed
tl-hw-prepare /dev/sdX tl-xxxx
```

### Reboot

{{% /steps %}}

### Legacy method

{{% steps %}}

### Create an unpack directory

On the USB drive, create an unpack directory:

```bash
mkdir -p /srv/unpack
```

### Update the system

Run the update or prepare the system using the `STAGE`
environment variable pointing to the unpack directory

```bash
STAGE=/srv/unpack tl-hw-prepare /dev/sdX tl-xxxx
```

### Clear

You can delete the newly unpacked subvolumes with

```bash
sudo rm -rf /srv/unpack/tmp-*
```

### Reboot

Reboot your system to complete the update `reboot`.

{{% /steps %}}
