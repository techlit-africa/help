+++
title = "Installing Component Updates"
linktitle = "Components"
weight = 2
+++

>[!WARNING]
> **Don't** follow this to install Desktop update
see [this](../desktop) instead

## Admin

{{% steps %}}

### Confirm

The update is avialable with

```bash
tl-comp-latest admin
```

### Install the update with

```bash
tl-archive-install admin
```

### Confirm update

Confirm the version is updated on the desktop info panel

{{% /steps %}}

## Curriculum

{{% steps %}}

### Confirm availablity

```bash
tl-comp-latest curriculum
```

### Install

To install curriculum use

```bash
tl-curriculum-install
```

### Confirm update completed

Reboot and confirm version is updated on the desktop info panel.
{{% /steps %}}
