+++
title = "Network Setup guidelines"
linktitle = "Network setup"
weight = 3
+++

This guide is for setting up the network for your class

{{% steps %}}

### Connect the Server

Begin by directly connecting a router to the server you might
need to reconfigure the desktop to be a server.

### Router Placement

If you’re using two routers;

* One router physically connects to the server,
the other one connects to the first one using an ethernet cable.
* keep the routers physically apart as much as possible.

### Ethernet for clients

Whenever possible, use Ethernet connections for clients to reduce load.

### Reconfigure

Ensure that computers are configured
for the nearest WiFi network.

### Disable WiFi on Ethernet

For the computers on ethernet (This includes `server`),
disable wifi, either by using the applet or hardware switch.

### Balance the load

If using multiple routers, distribute clients evenly across routers.

{{% /steps %}}

>[!IMPORTANT]
> * __Single Server Configuration__:
Ensure that only a single computer is configured as the server.
> * __Server setup__: Ensure that the server is not connected to Wifi (Step 5)
