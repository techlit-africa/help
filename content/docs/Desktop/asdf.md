+++
title = "Installing asdf"
linktitle = "asdf"
weight = 2
+++

How to setup [asdf](https://asdf-vm.com) package manager

0. ### Create /srv/guest

```bash
# Create the directory if it doesn't exist
mkdir /srv/guest

# Change permission
sudo chown guest:guest /srv/guest
```

## Copying from USB drive

{{% steps %}}

### Mount the USB

```bash
sudo mount /dev/sdX4 /mnt
# Replace X with the appropriate device letter
```

### Copy the tarball

> Do this as _guest_

`rsync -avP /mnt/guest/asdf.tar.gz /srv/guest/`

### Unpack and remove the tarball

```bash
cd /srv/guest

tar -xzvf asdf.tar.gz

rm asdf.tar.gz
```

### Unmount the USB drive

`sudo umount /mnt`

{{% /steps %}}

## Installing from github

> Do this as guest

{{% steps %}}

### Clone the repo

```bash
cd /srv/guest
git clone https://github.com/asdf-vm/asdf.git
```

### Add the plugins

```bash
asdf plugin-add ruby
asdf plugin-add haskell
asdf plugin-add java
asdf plugin-add clojure
```

### Install the specific versions

> The versions below are just examples... Install them only for uniformity

```bash
asdf install ruby 3.3.5
asdf install haskell 9.10-latest
asdf install java openjdk-23
asdf install java 1.11.4.1474
```

### Create a tarball

```bash
tar -czvf asdf.tar.gz asdf
```

> You can pull the file within the class network

{{% /steps %}}
