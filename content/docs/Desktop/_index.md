+++
title = "Desktop"
weight = 3
+++

## System Configuration

>[!WARNING] Don't forget to apply after changing any setting.

{{< cards >}}
  {{< card title="From start menu" image="./reconfigure.jpg" method="Resize" options="1200x q80" >}}
  {{< card title="Reconfigure terminal" image="./reconfigure_terminal.jpg" subtitle="The menu" >}}
{{< /cards >}}

>[!TIP]
> Use the arrow keys to navigate, <kbd>Space</kbd> to activate a setting,
and <kbd>Enter</kbd> to apply.

{{% details title="Click here to See available options" closed="true" %}}

- __Role__: Choose either `client` or `server`.
- __Name__: Set the computer hostname (e.g., `tl-1234`).
- __Wi-Fi__: Specify the WiFi SSID (case-sensitive).
- __GPU Hacks__: Provides workarounds for different graphics cards
- __WIFI Hacks__: Provides workarounds for different network cards
{{% /details %}}

## Using Hacks

Hacks provide essential workarounds to help your hardware function at its best.
Here are some situations where enabling hacks is necessary

1. __Graphics__: If you notice slow graphics performance or random freezes,
try another gpu hack
2. __Wi-Fi__: If your device can't connect to Wi-Fi, enabling a different Wi-Fi
hack(s) can resolve network issues.

>[!IMPORTANT] You can enable multiple hacks simultaneously.

## Managing with Control

The Control page is your hub for managing classes within the Techlit system.
Access the control page for managing classes

- Desktop: Launch from the start menu.
- Web Browser: Visit [Control](http://192.168.1.1/control "Control Page")
from any device on the Techlit Wi-Fi network.

## Next

{{< cards >}}
 {{< card link="network" title="Setup Network" icon="wifi" >}}
 {{< card link="updating" title="Updating" icon="refresh" >}}
{{< /cards >}}
