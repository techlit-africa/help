+++
title = "Admin Scripts"
linkTitle = "Admin"
weight = 4
draft = false
+++

These are the script you will be using to maintain your
TechLit System(fresh install, upgrade, reinstall, etc.)

{{% details title="__NOTE__ " closed="true" %}}

- __Run as admin__: You need to be admin user to run the script.
- __Basic Usage__: To run a script, use the following format:
`tl-group-command` parameters (e.g., `tl-hw-prepare /dev/sdb tl-2054`)
- __HELP__: Run the command without any arguments to print a quick usage.
- __Environment Variables__: Customize behavior with environment variables like:
`WIFI_NAME='My Network' tl-hw-prepare /dev/sda tl-2054`
{{< /details >}}

## Script format

The scripts follow a naming convention to make them easy to identify and use

- All admin scripts follow a consistent format: `tl-*-*`.
- Each script begins with the __prefix__ `tl-`.
- The second part represents the __group__ of commands it belongs
to (e.g., `hw`, `img`, `comp`, `sys`, or `usb`).
- Finally, the third part specifies the specific __action__ or command
within that group (e.g., `prepare`, `update`, etc.).

## Groups of commands

- The admin scripts are organized into distinct groups, each serving a specific purpose.

{{< tabs items="Hardware,Image,Component,System">}}
{{% tab %}}

### [ `tl-hw-*` ]

Hardware related commands (partitioning, installing components, updating bootloaders).

- `tl-hw-prepare`: __Creates__ partitions and installs components for TechLit system.

- Takes hard drive and tag as parameters.
- Example `tl-hw-prepare /dev/sdX tl-2054`

- `tl-hw-update-desktop`: Unpacks Techlit system archive with the version
provided into already-provisioned desktop system on `/dev/sdX`
  - Takes hard drive and version(optional) as parameters. example
  `tl-hw-update-desktop /dev/sdX`
- `tl-hw-update-bootloader`: Installs and configures bootloaders for the on `/dev/sdx`
- `tl-hw-chroot-desktop`: Mounts required partitions and chroots into
TechLit system on `/dev/sdx`
{{% /tab %}}

{{% tab %}}

### [ `tl-img-*` ]

Desktop image management (creating snapshots, packing/unpacking, restoring).

1. `tl-img-save`: Creates a new snapshot of the TechLit system hosted at `/var/btrfs/snapshots`.
2. `tl-img-pack`: Creates a new clone-able archive of the TechLit system hosted.
3. `tl-img-unpack`: Unpacks/Extracts a desktop archive into the TechLit system.
4. `tl-img-restore`: Restores a desktop snapshot of the TechLit system.
{{% /tab %}}

{{% tab %}}

### [ `tl-comp-*` ]

Component management (pulling, pushing, and syncing components).

- All the components are hosted in `/srv/$component`
- Components include (but not limited to)
`desktop`, `admin`, `curriculum`, `help` `control`

1. `tl-comp-pull`: Downloads a version of component from `remote`
2. `tl-comp-push`: Uploads a version of component from `/srv/component/` to `remote`
3. `tl-comp-sync`: Updates list of local and remote versions for component.
(This script is called by the other 2 scripts in this group)
{{% /tab %}}

{{% tab %}}

### [ `tl-sys-*` ]

System configuration (network setup, Driver personalization etc).

1. `tl-sys-configure`: Reconfigures Techlit system with a new configuration file.
    - The options include: `role`, `hostname`, `wifi_name`, and `wifi_password`
    - The config file is located here `/srv/secure/config.json`
2. `tl-sys-personalize`: Personalizes the system using the values in the `config.json`
3. `tl-sys-anonymize`: Removes sensitive data from a Techlit system and custom configuration.
{{% /tab %}}

{{% tab %}}
[ `tl-usb-*` ]
USB drive tasks (creating installer USBs, updating components).

- `tl-usb-prepare`: Creates and installs components a TechLit installer USB on `/dev/sdx.`
- `tl-usb-update-components`: Copies updated Techlit components on the USB.
- `tl-usb-update-recovery`: Updates iso partition with a new recovery iso.
- `tl-usb-update-bootloader`: Install/Update and configure bootloaders for the USB.

{{% /tab %}}
{{< /tabs >}}

## Examples

Some examples of some common command usage

### Install

- A Techlit system for the first time on a computer
with a hard drive `/dev/sda` and tag `tl-2054`

```bash
tl-hw-prepare /dev/sda tl-2054
```

- With environment variables

```bash
WIFI_NAME="TechLit Vim" tl-hw-prepare /dev/sda tl-2054
```

### Update

- A Techlit system from a desktop image hosted in `/dev/sda`

```bash
tl-hw-update-desktop /dev/sda
```

### Reconfigure

- A TechLit system first time after preparing

```bash
tl-sys-configure
```

### Fix Bootloader

- On a TechLit Computer unable to boot because of bootloader or wrong fstab by
reinstalling Grub bootloader on a TechLit system installed at `/dev/sda`

```bash
sudo tl-hw-update-bootloader /dev/sda
```
