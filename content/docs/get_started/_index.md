+++
title = 'Getting Started'
linktitle = 'Startup Guide'
date = 2024-02-02T21:36:21+03:00
draft = false
weight = 1
categories = ["Installation" ]
tags = "Installer"
+++

## Booting The USB Drive

>To __Boot__ from the USB Drive You need to access the
BIOS or UEFI __One-Time__ Boot menu.

{{% steps %}}

### Power off the computer

### Plug in the provided USB drive into your computer

### Open the BIOS/UEFI Boot menu

{{% details title="__Accessing the BIOS/UEFI Boot menu__" closed="true" %}}

- __Most devices__: Repeatedly press `F12`
immediately after pressing the power button during the startup process.
- __Apple__: Hold the `Alt` key before pressing the power button.
Let go when the menu appears.
- __HP__: Keep pressing `F9` right after you press the power button.
- __ChromeBooks__: Press `ESC` repeatedly once you turn on your computer.
- __Surface Laptops__: Press and hold the `Volume Down` button,
then press the power button.
{{% /details %}}

### Boot the USB

Select USB drive as the primary boot device and press Enter.
> Sometimes you will find duplicate USB entries, select whichever works.
{{% /steps %}}

Once successfully booted,
You should see a screen with a menu with a few examples.

![TechLit Installer](launch.jpg)

>[!TIP]
> - Always prefer the UEFI option if it's available in the boot menu.
> - IF you can't access the BIOS or UEFI Boot menu, using the mentioned
keyboard shortcuts, google it. Example "*Lenovo Thinkpad T420 Boot Menu*"
> - Troubleshoot booting issues [here](troubleshoot).

## Next

{{< cards cols="1" >}}
 {{< card link="install" title="Install / Update" icon="arrow-right" >}}
{{< /cards >}}
