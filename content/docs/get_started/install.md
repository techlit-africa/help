+++
linktitle = 'Install'
title = 'Install or Update TechLit System'
draft = false
weight = 2
categories = ["Installation" ]
tags = "Installer"
+++

> [!warning]
Follow [__minis__](../../updating/desktop#mini) for computers with < __32GB__
drive. If you try this you will run out of storage and the process will fail.

> [!NOTE]
> - use `lsblk` to see the list of drives available.
> - `/dev/sdX` and `/dev/nvme0n1` represent storage drives in the system.
> - `tl-xxxx` is the device tag
> - Customize config with [environment](#examples) variables

### Install (First time)

```bash {linenos=false}
tl-hw-prepare /dev/sdX tl-xxxx
```

>[!caution] This will wipe the hard drive. __You will lose public storage__

### Update

```bash {linenos=false}
tl-hw-update-desktop /dev/sdX
```

You should see a screen like this once done.

![Update Complete](done.jpg)

Once done, simply reboot the system by running `reboot` or
by long pressing the power button.

### Examples

- Install TechLit System on a computer with tag `tl-0606` and `nvme0n1`
as its hard drive.

```bash {linenos=false}
tl-hw-prepare /dev/nvme0n1 tl-0606
```

- Install TechLit System on a computer with tag `tl-0606` and `nvme0n1`
as its hard drive, setting Wi-Fi name

```bash {linenos=false}
WIFI_NAME="TechLit Vim" tl-hw-prepare /dev/nvme0n1 tl-0606

# Or
export WIFI_NAME="TechLit Vim"
tl-hw-prepare /dev/nvme0n1 tl-0606
```

- Update TechLit System on a computer with `/dev/sda` as the hard drive

```bash {linenos=false}
tl-hw-update-desktop /dev/sda
```

> [!NOTE]
> Unplug the USB drive immediately after rebooting
to avoid booting back into the installer.
