+++
title = 'Installation Issues'
linktitle = 'Troubleshoot'
date = 2024-02-02T23:30:47+03:00
draft = false
weight = 4
categories = ["Troubleshooting"]
tags = "Installer"
+++

To troubleshoot installation issues, you might need to access the BIOS menu.
Follow these steps for different manufacturers:

- __DELL__: Press <kbd>F2</kbd> or <kbd>F12</kbd> repeatedly before powering on.
- __HP__ <kbd>F10</kbd> or <kbd>ESC</kbd>
- __Acer and Lenovo__ <kbd>F1</kbd> or <kbd>F2</kbd>

A typical BIOS Menu looks like this

![BIOS Menu](dell_bios.jpg)
An example of Dell BIOS Menu

## Hardware issues

### USB fails to boot

I plugged in the USB and followed the correct procedure,
but my computer fails to boot from the USB drive even though
I can see the drive in the boot menu. The computer does one of the following:

{{< callout type="info" >}}

1. Boot the installed OS directly
2. Goes into a POST boot loop
3. Start a BIOS operation like memory test.
{{< /callout >}}

- USB Port Issue: Try using a USB 3.0 port instead of a USB 2.0 port.

- BIOS Settings: Ensure USB booting is enabled in the BIOS.
Change or disable UEFI or BIOS boot mode if necessary, and restart the computer.
Switching to a USB 3.0 port usually fixes the issue.

### USB Drive not showing up in Boot Menu

If your USB drive doesn't appear in the boot menu:

- Try using a different USB port.
- Check if USB booting is enabled in the BIOS. If not, enable it.
- Manually add the USB boot option in the BIOS settings.

## Admin issues

### Mistakes During Preparation or Update

If you made a mistake during the update or preparation process:

1. For tag, WiFi name, or WiFi password mistakes,
use the reconfigure option on the desktop.
2. For other issues like wrong disk drive,
simply rerun the script with the correct parameters.

### Further Help

If you're still facing difficulties:

1. Steps Taken: List the specific commands you ran (e.g., tl-hw-prepare ...).
2. Error Messages: Note any specific error messages or describe the computer's behavior
(e.g., "Freezes after showing framebuffer").
3. Hardware Details: Include the computer brand, model, GPU
