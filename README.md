# Repo for the Techlit System Desktop help

## Requirements

- hugo at least v0.141.0/extended (asdf version won't work!!)
- git
- go (To setup the modules)

## Setup

### Clone this repo

```bash
git clone https://github.com/techlit-africa/help.git

# Set up the theme
hugo mod get
hugo mod tidy
```

### Run the server

```bash
hugo server --disableFastRender
```
