{
  description = "Setup hugo dev env";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

  outputs = {
    self,
    nixpkgs,
  }: let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
  in {
    devShells.${system}.default = pkgs.mkShell {
      packages = with pkgs; [go hugo markdownlint-cli zsh tmux];

      shellHook = ''
        # hugo mod get -u
        # hugo server --buildDrafts --buildFuture --disableFastRender >/dev/null &
        export SHELL=${pkgs.zsh}

        tmux new-session -d -s help
        tmux attach -t help
      '';
    };
  };
}
